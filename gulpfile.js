/*jshint node:true*/

// Generated on <%= (new Date).toISOString().split('T')[0] %> using
// <%= pkg.name %> <%= pkg.version %>
/**
 *  Welcome to your gulpfile!
 *  Generated on <%= (new Date).toISOString().split('T')[0] %> using
 *  <%= pkg.name %> <%= pkg.version %>
 *  The gulp tasks are splitted in several files in the gulp directory
 *  because putting all here was really too long
 */
'use strict';

var gulp = require('gulp');
var wrench = require('wrench');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var del = require('del');

/**
 *  This will load all js or coffee files in the gulp directory
 *  in order to load all gulp tasks
 */
wrench.readdirSyncRecursive('./gulp').filter(function(file) {
  return (/\.(js|coffee)$/i).test(file);
}).map(function(file) {
  require('./gulp/' + file);
});

// Minify images
gulp.task('minify-images', function(){
  return gulp.src('app/assets/images/**/*.+(png|jpg|gif|svg)')
    .pipe(cache(imagemin({
      interlaced: true
    })))
    .pipe(gulp.dest('dist/images'))
});

// Minify css
gulp.task('minify-css', function(){
  return gulp.src('src/**/*.css')
    .pipe(useref())
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest('dist'))
});

// Minify js
gulp.task('minify-js', function(){
  return gulp.src(['src/**/*.js'])
    .pipe(useref())
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulp.dest('dist'))
});

// Move fonts
gulp.task('dist-fonts', function() {
  return gulp.src(['src/**/fonts/*', 'bower_components/bootstrap/dist/fonts/*'])
    .pipe(gulp.dest('dist/fonts'))
});

// Cache clear
gulp.task('clean:dist', function() {
  return del.sync('dist');
});

// Production mode
gulp.task('prod', ['clean:dist', 'dist-fonts', 'minify-images', 'minify-css', 'minify-js'], function() {
  return gulp.start('build').start('serve:dist');
});

// Developer mode
gulp.task('dev', ['clean:dist', 'dist-fonts', 'dist-jquery'], function() {
  return gulp.start('build').start('serve');
});

/**
 *  Default task clean temporaries directories and launch the
 *  main optimization build task
 */
gulp.task('default', ['clean'], function () {
  gulp.start('build');
});
