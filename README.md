# TMD Test task

![N|Solid](http://dl2.joxi.net/drive/2016/09/18/0004/1376/279904/04/50d8dde147.jpg)
![N|Solid](http://dl1.joxi.net/drive/2016/09/18/0004/1376/279904/04/27af74b0fe.jpg)

### Installation
Install dependencies
```sh
$ npm install
$ bower install
```
For production environments...
```sh
$ gulp prod
```

For development environments...
```sh
$ gulp dev
```

