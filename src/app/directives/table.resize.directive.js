(function () {
  'use strict';

  angular
    .module('tmd')
    .directive('ngTableResize', ngTableResize);

  /** @ngInject */
  function ngTableResize() {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        // unwrap $(element)
        // debugger;
        element.resizableColumns(scope.$eval(attrs.ngTableResize));

      }
    };
  }
})();
