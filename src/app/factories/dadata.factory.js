(function () {
  'use strict';

  angular
    .module('tmd')
    .factory('DadataFactory', DadataFactory);

  /** @ngInject */
  function DadataFactory($http) {
    var dataFactory = {};
    dataFactory.getBeers = function () {
      return $http.get('datasource/beers.json');
    };

    return dataFactory;
  }
})();
