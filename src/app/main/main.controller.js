(function () {
  'use strict';

  angular
    .module('tmd')
    .controller('MainController', MainController);

  MainController.$inject = ['$scope', 'DadataFactory'];
  /** @ngInject */
  function MainController($scope, DadataFactory) {
    DadataFactory.getBeers().then(function (beers) {
      $scope.beers = beers.data;
    }).catch(function (e) {
      throw e;
    });

  }
})();
